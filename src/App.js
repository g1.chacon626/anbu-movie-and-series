import React from 'react';
import logo from './logo.svg';
// import './App.css';
import Navbar from './components/nav_bar/Navbar';
import Hero from './components/hero/Hero';
import 'bootswatch/dist/lux/bootstrap.css';
function App() {
  return (
    <div>
      <Navbar/>
      <Hero/>
    </div>
  );
}

export default App;
