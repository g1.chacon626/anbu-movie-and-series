import React from "react";
import styles from "./css/Hero.module.css";
import "bootswatch/dist/lux/bootstrap.css";

class Hero extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <div className={styles["container-hero"]}>
          <div className={styles["img-hero"]}></div>
          <div className="card-body">
            <h2>
              ANBU <br />
              Movie & series
            </h2>
            <p>
              Encuentra las mejores series y peliculas en estreno para disfrutar
              desde la comodidad de su casa Proximamente figuras coleccionables.
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id
              sollicitudin lectus. Nulla scelerisque tempor libero quis dapibus.
              Donec ac eros finibus dolor sollicitudin pulvinar. Pellentesque
              commodo quam lectus, nec pellentesque nisl posuere eget. Duis
              sagittis, erat ut consectetur maximus, nunc nunc auctor ante, id
              auctor dolor purus eget magna. Nunc est ex, vestibulum vel erat
              vitae, mattis gravida ligula. Duis iaculis convallis congue.
              Praesent at imperdiet lorem. Praesent nisl libero, aliquam eget
              aliquet sit amet, sagittis vel ligula.
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default Hero;
