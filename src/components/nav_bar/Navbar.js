import React from 'react';
import styles from './css/Navbar.module.css'

class Navbar extends React.Component {
    
    constructor(){
        super()
        this.state = {
            active : false
        }
        const METHODS = [
            'handlerButtonNavbar'
        ]
        METHODS.forEach((method)=>{
            this[method] = this[method].bind(this)
        })
    }

    handlerButtonNavbar(){
        console.log("haciendo click")
        console.log("valor del state : " , this.state.active);
        if (this.state.active){
            this.setState({active : false})
        }else{
            this.setState({active : true})
        }
    }

    render(){
        const hasActive = this.state.active ? true : false;
        const activeClass = hasActive ? styles['show-navbar'] : styles['hide-navbar'];
        var clases = "";
        // if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        //     clases = styles['navbar-container'] + ' ' + activeClass;    
        // }else{
        //     clases = styles['navbar-container'];
        // }
        clases = styles['navbar-container'] + ' ' + activeClass;    
        return (
            <div>
                <div className={styles['container-hamburguer']}>
                    <div className={styles['button-hamburguer']}> 
                        <i className="fas fa-bars"  onClick={this.handlerButtonNavbar}></i>
                    </div>
                </div>
                

                <div className={styles['container-main-sticky']}>
                    <div className={clases}>
                        <div className={styles['navbar-items']} id={styles['ico-home']}></div>
                        <div className={styles['navbar-items']} id={styles['navbar-title']}><span>ANBU</span> Movies and Series</div>
                        <div className={styles['navbar-items']} id={styles['home']}>Home</div>
                        <div className={styles['navbar-items']} id={styles['products']}>Productos</div>
                        <div className={styles['navbar-items']} id={styles['contact']}>Contactenos</div>
                    </div>
                </div>
            </div>
            
        )
    }
}
export default Navbar;